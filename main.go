package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	. "github.com/reginaldoMorais/api-go/config"
	. "github.com/reginaldoMorais/api-go/config/dao"
	userrouter "github.com/reginaldoMorais/api-go/router"
)

var dao = UsersDAO{}
var config = Config{}

func init() {
	config.Read()

	dao.Host = config.Host
	dao.Database = config.Database
	dao.Connect()
}

func main() {
	r := mux.NewRouter()

	api := r.PathPrefix("/api/v1").Subrouter()
	api.HandleFunc("/users", userrouter.GetAll).Methods("GET")
	api.HandleFunc("/users/{id}", userrouter.GetByID).Methods("GET")
	api.HandleFunc("/users", userrouter.Create).Methods("POST")
	api.HandleFunc("/users/{id}", userrouter.Update).Methods("PUT")
	api.HandleFunc("/users/{id}", userrouter.Delete).Methods("DELETE")

	var port = ":8080"
	fmt.Println("Server running in port:", port)
	log.Fatal(http.ListenAndServe(port, r))
}
