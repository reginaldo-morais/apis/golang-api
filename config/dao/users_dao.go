package dao

import (
	"log"

	. "github.com/reginaldoMorais/api-go/models"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type UsersDAO struct {
	Host     string
	Database string
}

var db *mgo.Database

const (
	COLLECTION = "users"
)

func (m *UsersDAO) Connect() {
	session, err := mgo.Dial(m.Host)
	if err != nil {
		log.Fatal(err)
	}
	db = session.DB(m.Database)
}

func (m *UsersDAO) GetAll() ([]User, error) {
	var users []User
	err := db.C(COLLECTION).Find(bson.M{}).All(&users)
	return users, err
}

func (m *UsersDAO) GetByID(id string) (User, error) {
	var user User
	err := db.C(COLLECTION).FindId(id).One(&user)
	return user, err
}

func (m *UsersDAO) Create(user User) error {
	err := db.C(COLLECTION).Insert(&user)
	return err
}

func (m *UsersDAO) Delete(id string) error {
	err := db.C(COLLECTION).RemoveId(id)
	return err
}

func (m *UsersDAO) Update(id string, user User) error {
	err := db.C(COLLECTION).UpdateId(id, &user)
	return err
}
