module api-go

go 1.13

replace github.com/reginaldoMorais/api-go => ../api-go

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/reginaldoMorais/api-go v1.0.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
